name := "wordcounts"

version := "0.1"

scalaVersion := "2.12.2"


libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.2.0" % "provided"
// https://mvnrepository.com/artifact/org.apache.spark/spark-sql
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.2.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-mllib
libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "2.2.0" % "provided"
