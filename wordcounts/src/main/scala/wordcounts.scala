package wordcounts

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.sqrt
import org.apache.spark.sql.functions._
// import org.apache.spark.ml.regression.LinearRegression
// import org.apache.spark.ml.feature.VectorAssembler
// import org.apache.spark.ml.linalg.Vectors

object WordCounts {

    def main(arg: Array[String]) = {

        val spark = SparkSession.builder().getOrCreate()
        val data = spark.read.option("header","true").option("inferScheme","true").format("csv").load("/home/kumar/Videos/spark/HYG-Database-master/hygdata_v3.csv")
        println(data.printSchema)  

        val df = data.select(data("id"), data("x"), data("y"), data("z"), sqrt(data("x") - data("y") - data("z")).as("A")  )
        println("-------------------------------" + (df.sort(desc("A")).show()))

        df.sort(desc("A")).write.format("csv").save("/home/kumar/outfile")

    }
    
}
 

object WordCounts {

   def main(arg: Array[String]) = {

       val spark = SparkSession.builder().getOrCreate()
       val data  = spark.read.option("header","true").option("inferScheme","true").format("csv").load("/home/kumar/Videos/spark/globalterrorismdb_0617dist.csv")
       println(data.printSchema)  

       val result = data.select("country", "iyear").groupBy("iyear").count().distinct
       // result.foreach(println)
       // println("-------------------------------" + result.show()
       result.write.format("txt").save("/home/kumar/outfile_terr")

  }
         
}
          
